<?php

namespace App\Formatter;

class PlainTextFormatter implements OutputFormatter
{

    public function format(array $data): string
    {
        $output = '';

        foreach ($data as $info => $result){
            $output .= $info.': '.$result."\n\n";
        }

        return $output;
    }
}