<?php
/**
 * Created by PhpStorm.
 * User: farid
 * Date: 18/05/2019
 * Time: 19:20
 */

namespace App\Formatter;


interface OutputFormatter
{
    public function format(array $data): string;
}