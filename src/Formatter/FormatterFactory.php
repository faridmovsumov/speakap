<?php

namespace App\Formatter;

use App\Reader\ExtensionFinder;

class FormatterFactory
{
    public static function create(string $outputFile): OutputFormatter
    {
        $extensionFinder = new ExtensionFinder();
        $extension = $extensionFinder->getExtensionByFileName($outputFile);

        switch ($extension) {
            case "txt":
                return new PlainTextFormatter();
            case "json":
                return new JsonFormatter();
        }

        throw new \InvalidArgumentException("Formatter for file with extension $extension not found");
    }
}