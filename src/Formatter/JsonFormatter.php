<?php

namespace App\Formatter;

class JsonFormatter implements OutputFormatter
{

    public function format(array $data): string
    {
        return json_encode($data, JSON_PRETTY_PRINT);
    }
}