<?php

namespace App;

class Record
{
    const ACTION_CHECK_OUT = 'check-out';

    const ACTION_CHECK_IN = 'check-in';

    /**
     * @var string
     */
    private $timestamp;

    /**
     * @var string
     */
    private $person;

    /**
     * @var string
     */
    private $isbn;

    /**
     * @var string
     */
    private $action;

    /**
     * Record constructor.
     * @param string $timestamp
     * @param string $person
     * @param string $isbn
     * @param string $action
     */
    public function __construct(string $timestamp, string $person, string $isbn, string $action)
    {
        $this->timestamp = $timestamp;
        $this->person = $person;
        $this->isbn = $isbn;
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    /**
     * @return string
     */
    public function getPerson(): string
    {
        return $this->person;
    }

    /**
     * @return string
     */
    public function getIsbn(): string
    {
        return $this->isbn;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}