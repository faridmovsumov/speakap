<?php

namespace App\Writer;

interface Writer
{
    public function write($content): void;
}