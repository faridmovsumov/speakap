<?php

namespace App\Writer;

class FileWriter implements Writer
{
    private $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function write($content): void
    {
        $result = file_put_contents($this->filename, $content);
        if (!$result) {
            throw new \InvalidArgumentException("Couldn't write data to file ".$this->filename);
        }
    }
}