<?php

namespace App;

use App\DataAnalyser\DataAnalyser;
use App\DataAnalyser\DefaultDataAnalyser;
use App\Formatter\FormatterFactory;
use App\Reader\BulkFileReader;
use App\Writer\FileWriter;

class Reporter
{
    /**
     * @var array
     */
    private $files;

    /**
     * @var DefaultDataAnalyser
     */
    private $dataAnalyser;

    public function __construct($files, DataAnalyser $dataAnalyser = null)
    {
        $this->files = $files;
        if ($dataAnalyser === null) {
            $this->dataAnalyser = new DefaultDataAnalyser();
        } else {
            $this->dataAnalyser = $dataAnalyser;
        }
    }

    public function report($outputFile): void
    {
        $records = $this->readDataFromFiles();
        $reportData = $this->analyseRecords($records);
        $fileContent = $this->formatData($outputFile, $reportData);
        $this->writeToFile($outputFile, $fileContent);
    }

    private function writeToFile(string $outputFile, string $fileContent)
    {
        $fileWriter = new FileWriter($outputFile);
        $fileWriter->write($fileContent);
    }

    private function readDataFromFiles()
    {
        $bulkFileReader = new BulkFileReader();
        return $bulkFileReader->read($this->files);
    }

    private function formatData($outputFile, $reportData): string
    {
        $outputFormatter = FormatterFactory::create($outputFile);
        $fileContent = $outputFormatter->format($reportData);
        return $fileContent;
    }

    private function analyseRecords($records): array
    {
        $this->dataAnalyser->setRecords($records);
        $reportData = $this->dataAnalyser->analyse($records);
        return $reportData;
    }
}