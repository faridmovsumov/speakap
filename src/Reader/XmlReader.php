<?php

namespace App\Reader;

use App\Record;
use DOMDocument;
use Sabre\Xml\Reader;

class XmlReader implements FileReader
{
    /**
     * @var string
     */
    private $file;

    public function read(): array
    {
        if(empty($this->file)){
            throw new \LogicException("Please set a file for reading");
        }
        $reader = new Reader();
        $reader->xml($this->getFileContent());
        $reader->setSchema(__DIR__ . "/../../data/xsd/records.xsd");
        $doc = new DOMDocument;

        $records = [];

        // move to the first <product /> node
        while ($reader->read() && $reader->name !== 'record') ;

        // now that we're at the right depth, hop to the next <product/> until the end of the tree
        while ($reader->name === 'record') {
            $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
            $timestamp = (string)$node->timestamp;
            $isbn = (string)$node->isbn;
            $person = (string)$node->person["id"];
            $action = (string)$node->action["type"];

            $record = new Record($timestamp, $person, $isbn, $action);
            $records[] = $record;
            $reader->next('record');
        }

        return $records;
    }

    public function setFile($file): void
    {
        if (!file_exists($file)) {
            throw new \InvalidArgumentException("File $file not found");
        }
        $this->file = $file;
    }

    private function getFileContent()
    {
        return file_get_contents($this->file);
    }
}