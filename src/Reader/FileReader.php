<?php

namespace App\Reader;

interface FileReader
{
    public function read(): array;

    public function setFile($file): void;
}