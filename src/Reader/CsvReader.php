<?php

namespace App\Reader;

use App\Record;
use League\Csv\Reader;

class CsvReader implements FileReader
{
    private static $requiredColumns = [
        "timestamp",
        "person",
        "isbn",
        "action"
    ];

    /**
     * @var string
     */
    private $file;

    public function read(): array
    {
        if(empty($this->file)){
            throw new \LogicException("Please set a file for reading");
        }
        $csv = Reader::createFromPath($this->file, 'r');
        $csv->setHeaderOffset(0);
        $rows = $csv->getRecords();
        $header = $csv->getHeader();
        $this->validate($header);

        $records = [];
        foreach ($rows as $row) {
            $records[] = new Record($row["timestamp"], $row["person"], $row["isbn"], $row["action"]);
        }

        return $records;
    }

    private function validate($header)
    {
        foreach (static::$requiredColumns as $requiredColumn) {
            if (!in_array($requiredColumn, $header)) {
                throw new \InvalidArgumentException("Invalid csv file. Required column $requiredColumn not found.");
            }
        }
    }

    public function setFile($file): void
    {
        $this->file = $file;
    }
}