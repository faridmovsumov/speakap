<?php

namespace App\Reader;

class ExtensionFinder
{
    public function getExtensionByFileName(string $fileName): string
    {
        return pathinfo($fileName, PATHINFO_EXTENSION);
    }
}