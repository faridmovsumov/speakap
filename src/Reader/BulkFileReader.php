<?php

namespace App\Reader;

class BulkFileReader
{
    public function read(array $files): array
    {
        $data = [];
        foreach ($files as $file) {
            $fileReader = FileReaderFactory::create($file);
            $records = $fileReader->read();
            $data = array_merge($data, $records);
        }

        return $data;
    }
}