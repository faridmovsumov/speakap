<?php

namespace App\Reader;

class FileReaderFactory
{
    public static function create($file): FileReader
    {
        $extensionFinder = new ExtensionFinder();
        $extension = $extensionFinder->getExtensionByFileName($file);

        switch ($extension) {
            case "csv":
                $fileReader = new CsvReader();
                break;
            case "xml":
                $fileReader = new XmlReader();
                break;
            default:
                throw new \InvalidArgumentException("FileReader for file $file with extension $extension not found");
        }

        $fileReader->setFile($file);
        return $fileReader;
    }
}