<?php

namespace App\DataAnalyser;

interface DataAnalyser
{
    public function analyse(): array;
    public function setRecords($records): void;
}