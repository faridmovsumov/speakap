<?php

namespace App\DataAnalyser;

use App\Record;

class DefaultDataAnalyser implements DataAnalyser
{
    const MESSAGE_PERSON_WITH_MOST_CHECKOUTS = 'Which person has the most checkouts (which person_id)';

    const MESSAGE_BOOK_WITH_LONGEST_CHECKOUT_TIME = 'Which book was checked out the longest time in total (summed up over all transactions)';

    const MESSAGE_HOW_MANY_BOOKS_CHECKED_OUT_AT_THE_MOMENT = 'How many books are checked out at this moment';

    const MESSAGE_WHO_HAS_THE_LARGEST_AMOUNT_OF_BOOKS = 'Who currently has the largest number of books';

    /**
     * @var Record[]
     */
    private $records = [];

    public function analyse(): array
    {
        if (empty($this->records)) {
            throw new \LogicException("Please provide records to analyse");
        }
        $result = [];
        $result[self::MESSAGE_PERSON_WITH_MOST_CHECKOUTS] = $this->getPersonsWithTheMostCheckouts();
        $result[self::MESSAGE_BOOK_WITH_LONGEST_CHECKOUT_TIME] = $this->getBookWithLongestCheckOutTime();
        $result[self::MESSAGE_HOW_MANY_BOOKS_CHECKED_OUT_AT_THE_MOMENT] = $this->getHowManyBooksCheckedOutAtTheMoment();
        $result[self::MESSAGE_WHO_HAS_THE_LARGEST_AMOUNT_OF_BOOKS] = $this->getPersonWithLargestAmountOfBooks();
        return $result;
    }

    public function setRecords($records): void
    {
        $this->records = $records;
    }

    public function getPersonWithLargestAmountOfBooks(): string
    {
        $checkedOutBookRecords = [];

        /** @var Record $record */
        foreach ($this->records as $record) {
            if ($record->getAction() === Record::ACTION_CHECK_OUT) {
                $checkedOutBookRecords[$record->getIsbn()] = $record;
            } else {
                if (isset($checkedOutBookRecords[$record->getIsbn()]) && $checkedOutBookRecords[$record->getIsbn()] instanceof Record) {
                    $checkedOutBookRecords[$record->getIsbn()] = false;
                }
            }
        }

        $bookCountPerPerson = [];

        foreach ($checkedOutBookRecords as $checkedOutBookRecord) {
            if ($checkedOutBookRecord instanceof Record) {
                if (!isset($bookCountPerPerson[$checkedOutBookRecord->getPerson()])) {
                    $bookCountPerPerson[$checkedOutBookRecord->getPerson()] = 0;
                } else {
                    $bookCountPerPerson[$checkedOutBookRecord->getPerson()]++;
                }
            }
        }

        if(empty($bookCountPerPerson)){
            return "";
        }

        $ids = array_keys($bookCountPerPerson, max($bookCountPerPerson));
        return implode(",", $ids);
    }

    public function getHowManyBooksCheckedOutAtTheMoment(): int
    {
        $checkedOutBooks = [];

        /** @var Record $record */
        foreach ($this->records as $record) {
            if ($record->getAction() === Record::ACTION_CHECK_OUT) {
                $checkedOutBooks[$record->getIsbn()] = true;
            } else {
                if (isset($checkedOutBooks[$record->getIsbn()]) && $checkedOutBooks[$record->getIsbn()] === true) {
                    $checkedOutBooks[$record->getIsbn()] = false;
                }
            }
        }

        $result = 0;

        foreach ($checkedOutBooks as $checkedOutBook) {
            if ($checkedOutBook === true) {
                $result++;
            }
        }

        return $result;
    }

    public function getPersonsWithTheMostCheckouts(): string
    {
        $checkoutCounts = [];

        /** @var Record $record */
        foreach ($this->records as $record) {
            if ($record->getAction() === Record::ACTION_CHECK_OUT) {
                if (!isset($checkoutCounts[$record->getPerson()])) {
                    $checkoutCounts[$record->getPerson()] = 0;
                } else {
                    $checkoutCounts[$record->getPerson()]++;
                }
            }
        }

        if (empty($checkoutCounts)) {
            throw new \LogicException("There is no any user with check-out");
        }

        if(empty($checkoutCounts)){
            return "";
        }

        $ids = array_keys($checkoutCounts, max($checkoutCounts));
        return implode(",", $ids);
    }

    /**
     * @return string
     */
    public function getBookWithLongestCheckOutTime(): string
    {
        $checkOuts = [];
        $checkIns = [];
        $timesByBooks = [];

        /** @var Record $record */
        foreach ($this->records as $record) {
            if ($record->getAction() === Record::ACTION_CHECK_OUT) {
                $checkOuts[$record->getIsbn()][$record->getPerson()] = $record->getTimestamp();
            } else {
                $checkIns[$record->getIsbn()][$record->getPerson()] = $record->getTimestamp();

                if (isset($checkOuts[$record->getIsbn()][$record->getPerson()])) {

                    $timeDiffInSeconds = strtotime($checkIns[$record->getIsbn()][$record->getPerson()]) - strtotime($checkOuts[$record->getIsbn()][$record->getPerson()]);

                    if (!isset($timesByBooks[$record->getIsbn()])) {
                        $timesByBooks[$record->getIsbn()] = $timeDiffInSeconds;
                    } else {
                        $timesByBooks[$record->getIsbn()] += $timeDiffInSeconds;
                    }

                    unset($checkOuts[$record->getIsbn()][$record->getPerson()]);
                    unset($checkIns[$record->getIsbn()][$record->getPerson()]);
                }
            }
        }

        if(empty($timesByBooks)){
            return "";
        }

        $isbns = array_keys($timesByBooks, max($timesByBooks));
        return implode(",", $isbns);
    }
}