<?php

namespace Test;

use App\Reader\ExtensionFinder;
use PHPUnit\Framework\TestCase;

class ExtensionFinderTest extends TestCase
{
    public function testGetExtensionByFileNameSuccess()
    {
        $extensionFinder = new ExtensionFinder();
        $this->assertEquals("txt", $extensionFinder->getExtensionByFileName("test.txt"));
        $this->assertEquals("json", $extensionFinder->getExtensionByFileName("test.json"));
    }
}