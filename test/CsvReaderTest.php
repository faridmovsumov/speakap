<?php

namespace Test;

use App\Reader\CsvReader;
use App\Record;
use PHPUnit\Framework\TestCase;

class CsvReaderTest extends TestCase
{
    /**
     * @expectedException \LogicException
     */
    public function testReadFailWithoutFile()
    {
        $csvReader = new CsvReader();
        $csvReader->read();
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testReadNonCsvFile()
    {
        $csvReader = new CsvReader();
        $csvReader->setFile(__DIR__ . '/data/plain.txt');
        $csvReader->read();
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testReadInvalidCsvFile()
    {
        $csvReader = new CsvReader();
        $csvReader->setFile(__DIR__ . '/data/invalid.csv');
        $csvReader->read();
    }

    public function testReadSuccess()
    {
        $csvReader = new CsvReader();
        $csvReader->setFile(__DIR__ . '/data/valid.csv');
        $records = $csvReader->read();
        $this->assertIsArray($records);
        foreach ($records as $record) {
            $this->assertInstanceOf(Record::class, $record);
        }
    }
}