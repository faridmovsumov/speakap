<?php

namespace Test;

use App\Formatter\JsonFormatter;
use App\Formatter\PlainTextFormatter;
use App\Reader\BulkFileReader;
use App\Record;
use PHPUnit\Framework\TestCase;

class BulkFileReaderTest extends TestCase
{
    public function testReadSuccess()
    {
        $bulkFileReader = new BulkFileReader();

        $files = [
            __DIR__.'/data/valid.xml',
            __DIR__.'/data/valid.csv',
        ];

        $records = $bulkFileReader->read($files);
        $this->assertIsArray($records);
        foreach ($records as $record){
            $this->assertInstanceOf(Record::class, $record);
        }
    }
}