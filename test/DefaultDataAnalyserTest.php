<?php

namespace Test;

use App\DataAnalyser\DefaultDataAnalyser;
use App\Record;
use PHPUnit\Framework\TestCase;

class DefaultDataAnalyserTest extends TestCase
{
    /**
     * @expectedException \LogicException
     */
    public function testAnalyseFailWithoutRecords()
    {
        $defaultDataAnalyser = new DefaultDataAnalyser();
        $defaultDataAnalyser->analyse();
    }

    public function testAnalyseSuccess()
    {
        $dummyRecord = new Record("2017-01-10T00:05:00+00:00", "1", "test", "check-out");
        $defaultDataAnalyser = new DefaultDataAnalyser();
        $defaultDataAnalyser->setRecords([$dummyRecord]);
        $result = $defaultDataAnalyser->analyse();
        $this->assertIsArray($result);
    }
}