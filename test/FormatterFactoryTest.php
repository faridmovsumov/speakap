<?php

namespace Test;

use App\Formatter\FormatterFactory;
use App\Formatter\JsonFormatter;
use App\Formatter\PlainTextFormatter;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FormatterFactoryTest extends TestCase
{
    public function testCreateSuccess()
    {
        $this->assertInstanceOf(JsonFormatter::class, FormatterFactory::create("test.json"));
        $this->assertInstanceOf(PlainTextFormatter::class, FormatterFactory::create("test.txt"));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateFail()
    {
        FormatterFactory::create("test.doc");
    }
}