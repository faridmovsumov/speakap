<?php

namespace Test;

use App\Formatter\JsonFormatter;
use App\Formatter\PlainTextFormatter;
use PHPUnit\Framework\TestCase;

class PlainTextFormatterTest extends TestCase
{
    public function testFormat()
    {
        $plainTextFormatter = new PlainTextFormatter();
        $result = $plainTextFormatter->format(["test"]);
        $this->assertIsString($result);
        $this->assertNotEmpty($result);
    }
}