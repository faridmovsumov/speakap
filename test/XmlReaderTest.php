<?php

namespace Test;

use App\Reader\CsvReader;
use App\Reader\XmlReader;
use App\Record;
use PHPUnit\Framework\Error\Warning;
use PHPUnit\Framework\TestCase;

class XmlReaderTest extends TestCase
{
    /**
     * @expectedException \LogicException
     */
    public function testReadFailWithoutFile()
    {
        $xmlReader = new XmlReader();
        $xmlReader->read();
    }

    public function testReadNonXmlFile()
    {
        $this->expectException(Warning::class);
        $xmlReader = new XmlReader();
        $xmlReader->setFile(__DIR__ . '/data/plain.txt');
        $xmlReader->read();
    }

    public function testReadInvalidXmlFile()
    {
        $this->expectException(Warning::class);
        $xmlReader = new XmlReader();
        $xmlReader->setFile(__DIR__ . '/data/invalid.xml');
        $xmlReader->read();
    }

    public function testReadSuccess()
    {
        $xmlReader = new XmlReader();
        $xmlReader->setFile(__DIR__ . '/data/valid.xml');
        $records = $xmlReader->read();
        $this->assertIsArray($records);
        foreach ($records as $record) {
            $this->assertInstanceOf(Record::class, $record);
        }
    }
}