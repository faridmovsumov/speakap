<?php

namespace Test;

use App\Reporter;
use PHPUnit\Framework\TestCase;

class ReporterTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->clearOutputFiles();
    }

    public function tearDown()
    {
        parent::setUp();
        $this->clearOutputFiles();
    }

    public function testReportSuccess()
    {
        $xmlFile = __DIR__ . "/data/valid.xml";
        $csvFile = __DIR__ . "/data/valid.csv";
        $reporter = new Reporter([$xmlFile, $csvFile]);
        $txtOutputFile = __DIR__ . "/data/output/report.txt";
        $jsonOutputFile = __DIR__ . "/data/output/report.json";

        $reporter->report($txtOutputFile);
        $this->assertFileExists($txtOutputFile);
        $reporter->report($jsonOutputFile);
        $this->assertFileExists($jsonOutputFile);
    }

    private function clearOutputFiles(): void
    {
        $txtOutputFile = __DIR__ . "/data/output/report.txt";
        $jsonOutputFile = __DIR__ . "/data/output/report.json";

        if (file_exists($txtOutputFile)) {
            unlink($txtOutputFile);
        }

        if (file_exists($jsonOutputFile)) {
            unlink($jsonOutputFile);
        }
    }
}