<?php

namespace Test;

use App\Formatter\JsonFormatter;
use PHPUnit\Framework\TestCase;

class JsonFormatterTest extends TestCase
{
    public function testFormat()
    {
        $jsonFormatter = new JsonFormatter();
        $result = $jsonFormatter->format(["test"]);
        $this->assertIsString($result);
        $this->assertNotEmpty($result);
        $decoded = json_decode($result);
        $this->assertNotFalse($decoded);
    }
}