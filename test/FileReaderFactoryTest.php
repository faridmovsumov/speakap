<?php

namespace Test;

use App\Reader\CsvReader;
use App\Reader\FileReaderFactory;
use App\Reader\XmlReader;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FileReaderFactoryTest extends TestCase
{
    public function testCreateSuccess()
    {
        $this->assertInstanceOf(CsvReader::class, FileReaderFactory::create(__DIR__."/data/valid.csv"));
        $this->assertInstanceOf(XmlReader::class, FileReaderFactory::create(__DIR__."/data/valid.xml"));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testFileNotFound()
    {
        FileReaderFactory::create("test.txt");
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testUnsupportedType()
    {
        FileReaderFactory::create(__DIR__."/data/plain.txt");
    }
}