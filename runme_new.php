<?php

declare(strict_types=1);

use App\Reporter;

require_once "vendor/autoload.php";

$xmlFile = __DIR__."/data/input/2017-01.xml";
$csvFile = __DIR__ . "/data/input/2017-02.csv";
$reporter = new Reporter([$xmlFile, $csvFile]);
$reporter->report(__DIR__."/data/output/report.txt");
$reporter->report(__DIR__."/data/output/report.json");

echo "Success!\n";