Speakap Assignment
==================

**Author:** Farid Movsumov

## Installation

### Composer install

```bash
composer install
```

## How to run?
```bash
php runme_new.php
```

## How to run tests?

Run the following command in project root directory

```bash
vendor/bin/phpunit
```